// ? Operadores basicos, matematicas

    // ? Terminos: "unario", "binarios", "operados"

    // * Un operador: es a lo que se aplican los operadores. Por ejemplo, en la multiplicaion de 5 * 2
    // * hay dos operadores: el operador de la izquierda es 5 y el operador dereche es 2.
    // * A veces la gente lo llaman argumentos en lugar de operadores.

let x = 1;

x = -x;
console.log(x);

    // ? Matematicas
    // * Se admiten lo siguientes operadores matematicos:

        // * Suma +
        // * Sustraccion -
        // * Multiplicacion *
        // * Division /
        // * Resto %
        // * Exponencicion **

    // * Los primeros cuatro son sencillos, mientras el % y ** necesitan algunas palabras sobre ellos


    // ? Resto %
    // * El operador resto %, a pesar de su aparencia, no esta relacionado con porcentajes
    // * El resultado de a % b es el resto de la divicion entera de a por b
    // * Por ejemplo:

console.log("----------")
console.log( 5 % 2 );
console.log( 8 % 3 );
console.log( 8 % 4 );


    // ? Exponenciacion
    // * El operador de la exponenciacion a ** b aumenta a al poder de b.
    // * En matematicas escolares, lo escribimos como  ab
    // * Por ejemplo:

console.log("---------");
console.log( 2 ** 2 );
console.log( 2 ** 3 );
console.log( 2 ** 4 );

    // * Al igual que en las matematicas, el operador de exponenciacion tambien se define para numeros no enteros.
    // * Por ejemploi:

console.log("---------");
console.log( 8 ** (1/2) );
console.log( 8 ** (1/3) );