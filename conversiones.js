// conversion de cadenas
// * La conversion de cadenas ocurre cuando necesitamos la forma de cadena de una valor

let value = true;
console.log(typeof value);

value = String(value);
console.log(typeof value)

// conversion numerica
// * La conversion numerica en funciones y expresiones matematicas ocurre automaticamente

console.log( "6" / "2" );

// * podemos usar el Number(value) funcion para convertir explicitamente un value a un numero:

let str = "123";
console.log(typeof str);

let num = Number(str);
console.log(typeof num);

// ? si la cadena no es un numero valido, el resultado de dicha conversion es NaN, por ejemplo:

let age = Number("hola mundo");
console.log(age);

// * Reglas de conversion numerica

/* Valor                    se convierte...
? undefined                NaN
? null                     0
? true and false           1 y 0
? string                   Espacios en blanco (incluye espacios, tabulaciones \t, nuevas líneas \netc.) 
?                          desde el principio y el final se eliminan. Si la cadena restante está vacía, el 
?                          resultado es 0. De lo contrario, el número se "lee" de la cadena. Un error da NaN.
*/

// * ejemplos
console.log("------------")
console.log( Number("  123  ") );
console.log( Number("123z") );
console.log( Number(true) );
console.log( Number(false) );

// conversion booleana

// * La conversion booleana es la mas simple
// ? Ocurre en operaciones logicas pero tambien se puede realizar explicitamente con una llamada a Boolean(value).

/*
    ? La regla de conversion:
        ? * Valores que son intuitivamente "vacios", como 0, una cadena vacia, null, undefined, y NaN, se convierten a false.
        ? * Otros valores se conviertes en true
*/

